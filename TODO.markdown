# TODO

- escrever READMEs e Makefiles para projetos "complicados"
- colocar data de última modificação nos exemplos
  - estilosBibliografia_MMO
  - grafico_MMO
  - histogramas_VM
  - hyperlinks_MMO
  - idiomas_MMO
  - matriz_MMO
  - numeracaoPaginas_MMO
  - plotando-dados_MMO
  - plots_VM
  - preambulos_VM
  - quiverPlots_VM
  - relatorios_VM
- organizar exemplos por assunto


