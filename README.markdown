Exemplos de LaTeX
-----------------

Uma coleção de exemplos de  LaTeX, para pessoas com pouco conhecimento
do programa.

Leia antes de modificar
-----------------------

### Convenções

Os exemplos devem ser tão  simples quanto possível.  Procure, fornecer
apontadores para a  origem e inspiração dos exemplos.  Por exemplo, se
são cópia ou adaptação de algum trecho de manual, código encontrado na
internet.  É desejável  colocar  um  link para  o  manual dos  pacotes
usados, quando pertinente (por exemplo, no repositório CTAN).

### Novos exemplos

Se  o exemplo  requer  mais do  que a  compilação  usando `latex`  (ou
`pdflatex`),  ou também  se  há  vários arquivos  `.tex`  na pasta  do
exemplo,  é  importante  que  haja um  documento  `README`  na  pasta,
explicando os passos necessários para construir o documento.


Últimas notícias
----------------

Novos exemplos estão  sendo adicionados, em razão do  MOOC sobre LaTeX
que o Polignu está organizando


<!-- Local Variables: -->
<!-- fill-column: 70 -->
<!-- End: -->
