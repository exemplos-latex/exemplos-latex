// Obtem o endereco da pasta do arquivo main.sci e o seta como sendo a pasta atual do Scilab
CURDIR = get_absolute_file_path('logx.sce');
chdir(CURDIR);

//plotar log(x)
x = [1:0.1:10];
y = log(x);

//abrir um file descriptor de escrita para gravação 
fid = mopen("logx.dat", "w");
if (fid == -1)
    error('cannot open file for writing');
end
  
  for i = 1:size(x,"c")
      mfprintf(fid,"%f    %f\n",x(i),y(i));
  end  

mclose(fid);

