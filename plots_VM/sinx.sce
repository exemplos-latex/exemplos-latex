// Obtem o endereco da pasta do arquivo main.sci e o seta como sendo a pasta atual do Scilab
CURDIR = get_absolute_file_path('sinx.sce');
chdir(CURDIR);

//plotar sin(x)
x = [-3.14:0.01:3.14];
y = sin(x);

//abrir um file descriptor de escrita para gravação 
//fid = mopen("/home/vitor/Documentos/latex-testes/plots/data.dat", "w");
fid = mopen("sinx.dat", "w");
if (fid == -1)
    error('cannot open file for writing');
end
  
  for i = 1:size(x,"c")
      mfprintf(fid,"%f    %f\n",x(i),y(i));
  end  

mclose(fid);

